'use strict';
const superagent = require('superagent');

function askOverpass(codestring) {
  return superagent
    .post(`${process.env.OVERPASS_URL}/interpreter`)
    .send(codestring)
    .set('User-Agent', 'https://gitlab.com/webthings/webthing-osm/')
    .set('Accept', 'application/json');
}

async function getElementById(type, id) {
  if (!['node', 'way', 'relation'].includes(type)) throw new Error(`Unknown type: ${type}`)
  const query = `
  [out:json][timeout:25];
  (${type}(${id}););
  out body;>;out skel qt;
  `;
  const result = await askOverpass(query);
  const element = result.body.elements.find((element) => (
    element.id === id && element.type === type
  ));
  return element;
}

module.exports.askOverpass = askOverpass;
module.exports.getElementById = getElementById;

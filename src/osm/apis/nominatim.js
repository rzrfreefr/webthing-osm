'use strict';
const superagent = require('superagent');

function askNominatim(elementIds) {
  return superagent
    .get(`${process.env.NOMINATIM_URL}/lookup?format=json&osm_ids=${elementIds.join(',')}`)
    .set('User-Agent', 'https://gitlab.com/webthings/webthing-osm/')
    .set('Accept', 'application/json');
}

async function getElementById(type, id) {
  if (!['node', 'way', 'relation'].includes(type)) throw new Error(`Unknown type: ${type}`)
  const elementId = `${type.charAt(0).toUpperCase()}${id}`;
  const result = await askNominatim([elementId]);
  const element = result.body.find((element) => (
    element.osm_id === id && element.osm_type === type
  ));
  return element;
}

module.exports.askNominatim = askNominatim;
module.exports.getElementById = getElementById;

const cache = require('./cache.js');

describe('can store and receive', () => {
  const element = {
    id: 34,
  };
  beforeAll(() => {
    cache.saveToCache('node', element);
  });

  test('can receive the element', () => {
    expect(cache.getFromCache('node', 34)).toBe(element);
  });
  test('gets undefined on a different ID', () => {
    expect(cache.getFromCache('node', 33)).toBe(undefined);
  });
  test('gets undefined on a different type', () => {
    expect(cache.getFromCache('way', 34)).toBe(undefined);
  });
});

'use strict';
const express = require('express');
const { DateTime } = require('luxon');
const opening_hours = require('opening_hours');
const router = express.Router();


/**
* Create a date object which has the local time of a given timezone.
* @return {Date} date
*/
function fakeTimezoneForDate(date, timezone) {
  if (!date) {
    date = new Date();
  }
  return new Date(date.getTime() + getOffsetToLocal(timezone) * 60000);
}

function getOffsetToLocal(timezone) {
  const localDate = DateTime.local();
  const remoteDate = localDate.setZone(timezone);
  return remoteDate.offset - localDate.offset
}

function requestOpeningHours(element) {
  try {
    if (!element.opening_hours) throw new Error('No opening hours specified');
    const oh = new opening_hours(element.opening_hours, element.nominatimObject);
    const date = fakeTimezoneForDate(new Date(), element.timezone);
    return {
      open: oh.getState(date),
      unknown: oh.getUnknown(date),
      comment: oh.getComment(date) || '',
    };
  } catch(error) {
    return {
      open: false,
      unknown: true,
      comment: '',
    };
  }
}

router.get('/', async function (req, res) {
  const name = req.osmElement.name || 'POI';
  res.json({
    name: `${name}: Opening Hours`,
    type: 'thing',
    description: `The opening hours of ${name}`,
    properties: {
      open: {
        title: 'Open?',
        type: 'boolean',
        description: 'Whether the place is open',
        readOnly: true,
        href: `${req.baseUrl}/properties/open`,
      },
      unknown: {
        title: 'Unknown?',
        type: 'boolean',
        description: 'Whether the current state is unknown',
        readOnly: true,
        href: `${req.baseUrl}/properties/unknown`,
      },
      comment: {
        title: 'Comment',
        type: 'string',
        description: 'A comment for the current state',
        readOnly: true,
        href: `${req.baseUrl}/properties/comment`,
      },
    },
    actions: {},
    events: {},
    links: [
      {
        rel: 'properties',
        href: `${req.baseUrl}/properties`,
        mediaType: 'application/json',
      },
      {
        rel: 'actions',
        href: `${req.baseUrl}/actions`,
        mediaType: 'application/json',
      },
      {
        rel: 'events',
        href: `${req.baseUrl}/events`,
        mediaType: 'application/json',
      },
    ],
  });
});

router.get('/properties', function (req, res) {
  const {open, unknown, comment} = requestOpeningHours(req.osmElement);
  res.json({
    open,
    unknown,
    comment,
  });
});

router.get('/properties/open', function (req, res) {
  const {open} = requestOpeningHours(req.osmElement);
  res.json({open});
});

router.get('/properties/unknown', function (req, res) {
  const {unknown} = requestOpeningHours(req.osmElement);
  res.json({unknown});
});

router.get('/properties/comment', async function (req, res) {
  const {comment} = requestOpeningHours(req.osmElement);
  res.json({comment});
});

router.get('/actions', function (req, res) {
  res.json([]);
});

router.get('/events', function (req, res) {
  res.json([]);
});

module.exports = router;

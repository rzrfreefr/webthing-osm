'use strict';
const fs = require('fs');

const cache = {
  node: [],
  way: [],
  relation: [],
};

function loadFromFile() {
  let data = {};
  try {
    data = JSON.parse(fs.readFileSync('./data/cache.json', 'utf8'));
  } catch (error) {
    console.warn('Could not read element cache. Ignorning cache.');
    console.warn(error);
  }
  cache.node = data.node || [];
  cache.way = data.way || [];
  cache.relation = data.relation || [];
}

function saveToFile() {
  fs.writeFileSync('./data/cache.json', JSON.stringify(cache, null, 2));
}

function getFromCache(type, id) {
  return cache[type].find((element) => element.id === id);
}

function saveToCache(type, element) {
  cache[type].push(element);
  saveToFile();
}

module.exports.loadFromFile = loadFromFile;
module.exports.getFromCache = getFromCache;
module.exports.saveToCache = saveToCache;

'use strict';
const express = require('express');
const router = express.Router();
const geoTz = require('geo-tz');

const bundlerMiddleware = require('./bundler-middleware.js');
const askNominatimById = bundlerMiddleware(require('../apis/nominatim.js').getElementById);
const askOverpassById = bundlerMiddleware(require('../apis/overpass.js').getElementById);
const { loadFromFile, getFromCache, saveToCache } = require('./cache.js');

loadFromFile();

async function getOsmElement(type, id) {
  let element = getFromCache(type, id);
  if (!element) {
    console.log(`Asking server for ${type}(${id})`);
    const nominatimObject = await askNominatimById(type, id);
    const osmObject = await askOverpassById(type, id);
    const osmTags = osmObject.tags || {};
    element = {
      id: nominatimObject.osm_id,
      name: osmTags.name,
      opening_hours: osmTags.opening_hours,
      timezone: geoTz(nominatimObject.lat, nominatimObject.lon)[0],
      nominatimObject,
      osmObject,
    };
    if (element) saveToCache(type, element);
  }
  return element;
}

router.use('/', async function (req, res, next) {
  req.osmElement = await getOsmElement(req.osmType, req.osmId);
  if (!req.osmElement) {
    res.status(404).end('Element not found in OpenStreetMap');
    return;
  }
  next();
});

router.use('/opening-hours', require('./opening-hours/router-opening-hours.js'));

module.exports = router;
